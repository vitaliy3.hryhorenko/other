# TODO :ok_hand:

- ~~Ознакомиться с разметкой md - привести 10 примеров (список, заголовок, жирный шрифт, ~~зачеркивание~~, разделительная горизонтальная черта, раскрасит слово цветом) + 5 на свой выбор :sunglasses:~~
- Добавить разделы todo, список книг (внутри будет табличка с книгами), заголовок к заметки
- Прочитать немного больше по TDD, привести pros & cons данного подхода, продемонстрировать живой пример step by step
- Hotkey (распечатать)
- ~~https://www.youtube.com/watch?v=PZq_J1AuSL0~~
- https://www.youtube.com/watch?v=8u6_hctdhqI
- Оценка. Почитать статьи в интернете на тему оценек, задач по программированию

# Список книг :book:

* ~~[Идеальный программист. Как стать профессионалом разработки ПО](https://bass.netcracker.com/pages/viewpage.action?pageId=465865106)~~
* ~~[Программист фанатик](https://bass.netcracker.com/pages/viewpage.action?pageId=471699619)~~
* ```Текущая книга:``` [Spring in action. Fifth edition](https://github.com/ppatil9096/books/blob/master/Spring%20in%20Action%2C%205th%20Edition.pdf)
* [Базовые знания тестировщика веб-приложений](https://bass.netcracker.com/pages/viewpage.action?pageId=463529104)
* [Не заставляйте меня думать](https://bass.netcracker.com/pages/viewpage.action?pageId=520717801)
* [Java. Эффективное программирование](https://bass.netcracker.com/pages/viewpage.action?pageId=467209594)

***




# Заметки